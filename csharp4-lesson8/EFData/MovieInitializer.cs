﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using csharp4_lesson8.EFData;
using csharp4_lesson8.Models;
using csharp4_lesson8.ViewModels;
namespace csharp4_lesson8.EFData
{
    public class MovieInitializer : DropCreateDatabaseAlways<MovieContext>
    {
        protected override void Seed(MovieContext context)
        {
            var movies = new List<Movie>
            {
                new Movie{Title="Godfather", ReleaseYear=1972},
                new Movie{Title="Godfather Part 2", ReleaseYear=1973},
                new Movie{Title="Pulp Fiction", ReleaseYear=1994},
                new Movie{Title="Paths of Glory", ReleaseYear=1957},
                new Movie{Title="American Beauty", ReleaseYear=1999},
                new Movie{Title="Wall-E", ReleaseYear=2008},
                new Movie{Title="Godfather Part 3", ReleaseYear=1900},
                new Movie{Title="Citizen Kane", ReleaseYear=1941},
                new Movie{Title="Braveheart", ReleaseYear=1995},
                new Movie{Title="The Sting", ReleaseYear=1973},
                new Movie{Title="The Third Man", ReleaseYear=1949}
            };
            movies.ForEach(s => context.movies.Add(s));
            context.SaveChanges();
        }
    }
}
