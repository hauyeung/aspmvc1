﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using csharp4_lesson8.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
using csharp4_lesson8.EFData;
using csharp4_lesson8.ViewModels;

namespace csharp4_lesson8.Models
{
    public class MovieContext : DbContext
    {
        //
        // GET: /DBContext/
        public DbSet<Movie> movies { get; set; }
    }
}
