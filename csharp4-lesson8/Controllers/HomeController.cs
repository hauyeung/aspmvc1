﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson8.Models;
using csharp4_lesson8.ViewModels;

namespace csharp4_lesson8.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int i=1)
        {
            using (MovieContext movie = new MovieContext())
            {
                decimal numentries = movie.movies.ToList().Count;
                ViewBag.numpages = (int)Math.Ceiling(numentries / 5);
                Movies m = new Movies();
                m.mvs = movie.movies.OrderBy(x => x.Title).Skip(5 * (i - 1)).Take(5).ToList();
                return PartialView(m.mvs);
            }
        }

    }
}
